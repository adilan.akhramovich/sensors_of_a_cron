# import json

# # Open the JSON file
# with open('kask.json') as file:
#     # Load the JSON data
#     data = json.load(file)

# # Print the keys and their values in the JSON data
# for key, value in data.items():
#     print(key, value)



# import json
# import csv

# # Load the JSON file
# with open('kask.json') as file:
#     data = json.load(file)

# # Extract the necessary information from the JSON data
# complexes = data['complexes']

# # Create a CSV file and write the extracted data into it
# with open('kask.csv', 'w', newline='') as file:
#     writer = csv.writer(file)

#     # Write the header row
#     writer.writerow(['ID', 'Name', 'Address', 'Price'])

#     # Write the data rows
#     for complex_id, complex_data in complexes.items():
#         complex_name = complex_data['name']
#         complex_address = complex_data['address']
#         complex_price = complex_data['priceFrom']

#         writer.writerow([complex_id, complex_name, complex_address, complex_price])

# print("Conversion to CSV completed successfully.")


import json
import csv

# Load the JSON file
with open('kask.json') as file:
    data = json.load(file)

# Extract the necessary information from the JSON data
complexes = data['complexes']

# Create a CSV file and write the extracted data into it
with open('data.csv', 'w', newline='') as file:
    writer = csv.writer(file)

    # Write the header row
    writer.writerow(['ID', 'Name', 'Address', 'Price', 'State', 'Work From', 'Work To', 'Is Price Square Visible', 'Is Sales Completed', 'Country', 'Latitude', 'Longitude', 'Zoom'])

    # Write the data rows
    for complex_id, complex_data in complexes.items():
        complex_name = complex_data['name']
        complex_address = complex_data['address']
        complex_price = complex_data['priceFrom']
        state_text = complex_data['stateText']
        work_from = complex_data['workTime']['from']
        work_to = complex_data['workTime']['to']
        is_price_square_visible = complex_data['isPriceSquareVisible']
        is_sales_completed = complex_data['isSalesCompleted']
        country = complex_data['country']
        lat = complex_data['map']['lat']
        lon = complex_data['map']['lon']
        zoom = complex_data['map']['zoom']

        writer.writerow([complex_id, complex_name, complex_address, complex_price, state_text, work_from, work_to, is_price_square_visible, is_sales_completed, country, lat, lon, zoom])

print("Conversion to CSV completed successfully.")

