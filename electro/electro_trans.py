import psycopg2
import json

# Database connection details
db_user = 'rwh_analytics'
db_password = '4HPzQt2HyU@'
db_host = '172.30.227.205'
db_port = '5439'
db_name = 'rwh_gis_datalake'

# GeoJSON file path
geojson_file = 'Трансформаторы.geojson'

# Table name
table_name = 'electro_transformators'

# Read the GeoJSON file
with open(geojson_file) as file:
    data = json.load(file)

# Connect to the database
conn = psycopg2.connect(
    user=db_user,
    password=db_password,
    host=db_host,
    port=db_port,
    database=db_name
)

# Create a cursor object
cursor = conn.cursor()

# Create the table for the data
create_table_query = f"""
    CREATE TABLE IF NOT EXISTS {table_name} (
        id SERIAL PRIMARY KEY,
        name TEXT,
        district TEXT,
        geom GEOMETRY(Point, 4326)
    );
"""
cursor.execute(create_table_query)
conn.commit()

# Iterate over features and insert data into the table
for feature in data['features']:
    properties = feature['properties']
    geometry = feature['geometry']

    # Extract values from properties
    name = properties['NAME']
    district = properties['district']
    marker = properties['marker']

    # Insert the data into the table
    insert_query = f"""
        INSERT INTO {table_name} (name, district, geom)
        VALUES (%s, %s, ST_SetSRID(ST_GeomFromGeoJSON(%s), 4326));
    """
    params = (name, district, json.dumps(geometry))
    cursor.execute(insert_query, params)

# Commit the transaction and close the connection
conn.commit()
cursor.close()
conn.close()
