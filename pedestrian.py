import psycopg2
import json

# Database connection parameters
database = "rwh_gis_datalake"
user = "rwh_analytics"
password = "4HPzQt2HyU@"
host = "172.30.227.205"
port = "5439"

# Connect to PostgreSQL
conn = psycopg2.connect(
    database=database,
    user=user,
    password=password,
    host=host,
    port=port
)

# Create a cursor object to execute SQL statements
cursor = conn.cursor()

# Create the table if it does not exist
create_table_query = """
    CREATE TABLE IF NOT EXISTS cpedestrians (
        id SERIAL PRIMARY KEY,
        marker GEOMETRY(Point, 4326),
        area VARCHAR,
        type_of VARCHAR,
        address VARCHAR,
        additional VARCHAR,
        district VARCHAR
    )
"""
cursor.execute(create_table_query)
conn.commit()

# Load the GeoJSON data
with open('pedestrian.geojson') as f:
    data = json.load(f)

# Extract the features from the GeoJSON
features = data['features']

# Insert each feature into the table
for feature in features:
    properties = feature['properties']
    marker = properties['marker']
    geometry = json.dumps(feature['geometry'])

    insert_query = """
        INSERT INTO cpedestrians (
            marker,
            area,
            type_of,
            address,
            additional,
            district
        )
        VALUES (
            ST_SetSRID(ST_MakePoint(%(marker_lon)s, %(marker_lat)s), 4326),
            %(area)s,
            %(type_of)s,
            %(address)s,
            %(additional)s,
            %(district)s
        )
    """
    cursor.execute(insert_query, {"marker_lat": marker[1], "marker_lon": marker[0], **properties})
    conn.commit()

# Close the cursor and connection
cursor.close()
conn.close()
