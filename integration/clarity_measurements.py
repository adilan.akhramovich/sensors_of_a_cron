import requests
import psycopg2
import json

# Database connection details
db_user = 'rwh_analytics'
db_password = '4HPzQt2HyU@'
db_host = '172.30.227.205'
db_port = '5439'
db_name = 'rwh_gis_datalake'

# API details
base_url = "https://clarity-data-api.clarity.io/v1/measurements"
headers = {
    "x-api-key": "Z7RuEZeP30XBRI0AYCYYCuduaFiEk6UWfFWvjhOA",
    "Accept-Encoding": "gzip"
}

# Codes to fetch data for
codes = [
    "ABSVCS2G",
    "ACLGTYZP",
    "AS1ZZC74",
    "AWMTYN8S",
    "AP5Y6H38",
    "A0J5Z6HX",
    "A6XCF9G9",
    "ANHLZ5G3",
    "AYD65Y4W",
    "A9K21GBC",
    "A2D2S45H",
    "AWM1HWRL",
    "AW9LSVZ1",
    "A78K74FV",
    "A55MRV60",
    "AP28P3CM",
    "APN3LM47",
    "APQYTDMG",
    "A9R93M7W",
    "A2QXXHFK",
    "ATQ7CTGR",
    "A493HN85",
    "AYW6H92D",
    "ABS4NP3P",
    "AX6GRT4R",
    "AB10P5TQ",
    "AXZVRRY5",
    "AFCZP5M2",
    "ALPTNQLC",
    "ALJB642Z",
    "A3WMMLKH",
    "AJDDBT24",
    "A8LSCMJZ",
    "ARDXX6VF",
    "A0XVGM06",
    "APQCVRSB",
    "APXC2BSS",
    "AD9WZXVW",
    "ABHY2H3G",
    "ATKM31G2",
    "A45JCCLJ",
    "AXNJG9VL",
    "AP5MV3K3",
    "A1TJ77YY",
    "AZ4M16XT",
    "A7DMYSM9",
    "ANSZ3HZG",
    "ALC3D4G7",
    "AS049550",
    "A5SJ7ZKZ",
    "AJPMXS7D",
    "AYJDT581",
    "AJ0KDG9J"
]

# Database connection
conn = psycopg2.connect(database=db_name, user=db_user, password=db_password, host=db_host, port=db_port)
cur = conn.cursor()

# Create the measurements table if it doesn't exist
cur.execute("""
    CREATE TABLE IF NOT EXISTS clarity_measurements (
        code TEXT,
        time TIMESTAMP,
        location GEOMETRY(Point),
        characteristics JSONB
    )
""")

# Iterate over each code and fetch data
for code in codes:
    url = f"{base_url}?code={code}&limit=500"
    response = requests.get(url, headers=headers)
    data = response.json()

    # Insert data into the table
    for item in data:
        time = item.get('time')
        location = item.get('location', {}).get('coordinates', [])
        point = None
        if location:
            point = f"POINT({location[0]} {location[1]})"

        characteristics = json.dumps(item.get('characteristics')) if 'characteristics' in item else None

        cur.execute("""
            INSERT INTO clarity_measurements (
                code, time, location, characteristics
            ) VALUES (%s, %s, %s, %s)
        """, (
            code,
            time,
            point,
            characteristics
        ))

# Commit the changes and close the connection
conn.commit()
cur.close()
conn.close()
