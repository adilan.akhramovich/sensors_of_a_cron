# import requests
# import logging

# url = "https://clarity-data-api.clarity.io/v1/measurements?code=ACLGTYZP&limit=500"
# headers = {
#     "x-api-key": "Z7RuEZeP30XBRI0AYCYYCuduaFiEk6UWfFWvjhOA",
#     "Accept-Encoding": "gzip"
# }

# # Configure the logging module
# logging.basicConfig(filename='api_data.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# # Make the API request
# response = requests.get(url, headers=headers)
# data = response.json()

# # Log the data
# logging.info(f"API Response: {data}")

# # Further processing of the data as per your requirements
# ...

# ABSVCS2G
# ACLGTYZP
# AS1ZZC74
# AWMTYN8S
# AP5Y6H38
# A0J5Z6HX
# A6XCF9G9
# ANHLZ5G3
# AYD65Y4W
# A9K21GBC
# A2D2S45H
# AWM1HWRL
# AW9LSVZ1
# A78K74FV
# A55MRV60
# AP28P3CM
# APN3LM47
# APQYTDMG
# A9R93M7W
# A2QXXHFK
# ATQ7CTGR
# A493HN85
# AYW6H92D
# ABS4NP3P
# AX6GRT4R
# AB10P5TQ
# AXZVRRY5
# AFCZP5M2
# ALPTNQLC
# ALJB642Z
# A3WMMLKH
# AJDDBT24
# A8LSCMJZ
# ARDXX6VF
# A0XVGM06
# APQCVRSB
# APXC2BSS
# AD9WZXVW
# ABHY2H3G
# ATKM31G2
# A45JCCLJ
# AXNJG9VL
# AP5MV3K3
# A1TJ77YY
# AZ4M16XT
# A7DMYSM9
# ANSZ3HZG
# ALC3D4G7
# AS049550
# A5SJ7ZKZ
# AJPMXS7D
# AYJDT581
# AJ0KDG9J


import requests
import logging

codes = [
    "ABSVCS2G",
    "ACLGTYZP",
    "AS1ZZC74",
    "AWMTYN8S",
    "AP5Y6H38",
    "A0J5Z6HX",
    "A6XCF9G9",
    "ANHLZ5G3",
    "AYD65Y4W",
    "A9K21GBC",
    "A2D2S45H",
    "AWM1HWRL",
    "AW9LSVZ1",
    "A78K74FV",
    "A55MRV60",
    "AP28P3CM",
    "APN3LM47",
    "APQYTDMG",
    "A9R93M7W",
    "A2QXXHFK",
    "ATQ7CTGR",
    "A493HN85",
    "AYW6H92D",
    "ABS4NP3P",
    "AX6GRT4R",
    "AB10P5TQ",
    "AXZVRRY5",
    "AFCZP5M2",
    "ALPTNQLC",
    "ALJB642Z",
    "A3WMMLKH",
    "AJDDBT24",
    "A8LSCMJZ",
    "ARDXX6VF",
    "A0XVGM06",
    "APQCVRSB",
    "APXC2BSS",
    "AD9WZXVW",
    "ABHY2H3G",
    "ATKM31G2",
    "A45JCCLJ",
    "AXNJG9VL",
    "AP5MV3K3",
    "A1TJ77YY",
    "AZ4M16XT",
    "A7DMYSM9",
    "ANSZ3HZG",
    "ALC3D4G7",
    "AS049550",
    "A5SJ7ZKZ",
    "AJPMXS7D",
    "AYJDT581",
    "AJ0KDG9J"
]

url = "https://clarity-data-api.clarity.io/v1/measurements"
headers = {
    "x-api-key": "Z7RuEZeP30XBRI0AYCYYCuduaFiEk6UWfFWvjhOA",
    "Accept-Encoding": "gzip"
}

# Configure the logging module
logging.basicConfig(filename='api_data.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Make API requests for each code
for code in codes:
    params = {
        "code": code,
        "limit": 500
    }
    response = requests.get(url, headers=headers, params=params)
    data = response.json()
    logging.info(f"API Response for code '{code}': {data}")
