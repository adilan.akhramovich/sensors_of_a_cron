import requests
import psycopg2
import json

# Database connection details
db_user = 'rwh_analytics'
db_password = '4HPzQt2HyU@'
db_host = '172.30.227.205'
db_port = '5439'
db_name = 'rwh_gis_datalake'

# API details
url = "https://clarity-data-api.clarity.io/v1/devices"
headers = {
    "x-api-key": "Z7RuEZeP30XBRI0AYCYYCuduaFiEk6UWfFWvjhOA"
}

# Make the API request
response = requests.get(url, headers=headers)
data = response.json()

# Database connection
conn = psycopg2.connect(database=db_name, user=db_user, password=db_password, host=db_host, port=db_port)
cur = conn.cursor()

# Create table if not exists
cur.execute("""
    CREATE TABLE IF NOT EXISTS clarity_devices (
        location GEOMETRY(Point),
        enabledCharacteristics TEXT[],
        roadDistance FLOAT,
        aglHeight FLOAT,
        overallStatus TEXT,
        code TEXT,
        indoor BOOLEAN,
        workingStartAt TIMESTAMP,
        state JSONB,
        lifeStage TEXT,
        batteryStatus TEXT,
        signalStrength TEXT,
        batteryPercentage FLOAT,
        rssi FLOAT,
        photo TEXT,
        latestBatteryVoltage FLOAT,
        sensorsHealthStatus TEXT,
        lastReadingReceivedAt TIMESTAMP
    )
""")

# Insert data into the table
for item in data:
    location = item.get('location', {}).get('coordinates', [])
    point = None
    if location:
        point = "POINT(%s %s)" % (location[0], location[1])

    state = json.dumps(item.get('state')) if 'state' in item else None

    cur.execute("""
        INSERT INTO clarity_devices (
            location, enabledCharacteristics, roadDistance, aglHeight, overallStatus, code, indoor,
            workingStartAt, state, lifeStage, batteryStatus, signalStrength, batteryPercentage, rssi,
            photo, latestBatteryVoltage, sensorsHealthStatus, lastReadingReceivedAt
        ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    """, (
        point,
        item.get('enabledCharacteristics', []),
        item.get('roadDistance'),
        item.get('aglHeight'),
        item.get('overallStatus'),
        item.get('code'),
        item.get('indoor', False),
        item.get('workingStartAt'),
        state,
        item.get('lifeStage'),
        item.get('batteryStatus'),
        item.get('signalStrength'),
        item.get('batteryPercentage'),
        item.get('rssi'),
        item.get('photo'),
        item.get('latestBatteryVoltage'),
        item.get('sensorsHealthStatus'),
        item.get('lastReadingReceivedAt')
    ))

# Commit the changes and close the connection
conn.commit()
cur.close()
conn.close()
