import requests
import psycopg2

# Database connection details
db_user = 'rwh_analytics'
db_password = '4HPzQt2HyU@'
db_host = '172.30.227.205'
db_port = '5439'
db_name = 'rwh_gis_datalake'

# API details
url = "https://clarity-data-api.clarity.io/v1/datasources"
headers = {
    "x-api-key": "Z7RuEZeP30XBRI0AYCYYCuduaFiEk6UWfFWvjhOA"
}

# Make the API request
response = requests.get(url, headers=headers)
data = response.json()

# Database connection
conn = psycopg2.connect(database=db_name, user=db_user, password=db_password, host=db_host, port=db_port)
cur = conn.cursor()

# Create table if not exists
cur.execute("""
    CREATE TABLE IF NOT EXISTS clarity_datasources (
        subscriptionStatus TEXT,
        subscriptionExpirationDate TEXT,
        tags JSONB,
        group_name TEXT,
        name TEXT,
        datasourceId TEXT,
        sourceType TEXT,
        deviceCode TEXT
    )
""")

# Insert data into the table
for item in data:
    cur.execute("""
        INSERT INTO clarity_datasources (
            subscriptionStatus, subscriptionExpirationDate, tags, group_name, name, datasourceId, sourceType, deviceCode
        ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
    """, (
        item['subscriptionStatus'],
        item['subscriptionExpirationDate'],
        item['tags'],
        item['group'],
        item['name'],
        item['datasourceId'],
        item['sourceType'],
        item['deviceCode']
    ))

# Commit the changes and close the connection
conn.commit()
cur.close()
conn.close()
