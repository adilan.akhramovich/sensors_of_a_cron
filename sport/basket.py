import psycopg2
import json

# Database connection details
db_user = 'rwh_analytics'
db_password = '4HPzQt2HyU@'
db_host = '172.30.227.205'
db_port = '5439'
db_name = 'rwh_gis_datalake'

# GeoJSON file path
geojson_file = 'basket.geojson'

# Table name
table_name = 'basketball_field'

# Read the GeoJSON file
with open(geojson_file) as file:
    data = json.load(file)

# Connect to the database
conn = psycopg2.connect(
    user=db_user,
    password=db_password,
    host=db_host,
    port=db_port,
    database=db_name
)

# Create a cursor object
cursor = conn.cursor()

# Create the table for the data
create_table_query = f"""
    CREATE TABLE IF NOT EXISTS {table_name} (
        id SERIAL PRIMARY KEY,
        name TEXT,
        description TEXT,
        categories TEXT,
        address TEXT,
        address_comment TEXT,
        postal_code TEXT,
        microdistrict TEXT,
        district TEXT,
        city TEXT,
        district_round TEXT,
        region TEXT,
        country TEXT,
        opening_hours TEXT,
        timezone TEXT,
        rating FLOAT,
        review_count FLOAT,
        phone1 TEXT,
        phone2 TEXT,
        phone3 TEXT,
        email1 TEXT,
        email2 TEXT,
        email3 TEXT,
        website1 TEXT,
        website2 TEXT,
        website3 TEXT,
        instagram1 TEXT,
        instagram2 TEXT,
        instagram3 TEXT,
        twitter1 TEXT,
        twitter2 TEXT,
        twitter3 TEXT,
        facebook1 TEXT,
        facebook2 TEXT,
        facebook3 TEXT,
        vkontakte1 TEXT,
        vkontakte2 TEXT,
        vkontakte3 TEXT,
        whatsapp1 TEXT,
        whatsapp2 TEXT,
        whatsapp3 TEXT,
        viber1 TEXT,
        viber2 TEXT,
        viber3 TEXT,
        telegram1 TEXT,
        telegram2 TEXT,
        telegram3 TEXT,
        youtube1 TEXT,
        youtube2 TEXT,
        youtube3 TEXT,
        skype1 TEXT,
        skype2 TEXT,
        skype3 TEXT,
        latitude FLOAT,
        longitude FLOAT,
        gis_url TEXT,
        geom GEOMETRY(Point, 4326)
    );
"""
cursor.execute(create_table_query)
conn.commit()

# Iterate over features and insert data into the table
for feature in data['features']:
    properties = feature['properties']
    geometry = feature['geometry']

    # Extract values from properties
    name = properties['Наименование']
    description = properties['Описание']
    categories = properties['Рубрики']
    address = properties['Адрес']
    address_comment = properties['Комментарий к адресу']
    postal_code = properties['Почтовый индекс']
    microdistrict = properties['Микрорайон']
    district = properties['Район']
    city = properties['Город']
    district_round = properties['Округ']
    region = properties['Регион']
    country = properties['Страна']
    opening_hours = properties['Часы работы']
    timezone = properties['Часовой пояс']
    rating = properties['Рейтинг']
    review_count = properties['Количество отзывов']
    phone1 = properties['Телефон 1']
    phone2 = properties['Телефон 2']
    phone3 = properties['Телефон 3']
    email1 = properties['E-mail 1']
    email2 = properties['E-mail 2']
    email3 = properties['E-mail 3']
    website1 = properties['Веб-сайт 1']
    website2 = properties['Веб-сайт 2']
    website3 = properties['Веб-сайт 3']
    instagram1 = properties['Instagram 1']
    instagram2 = properties['Instagram 2']
    instagram3 = properties['Instagram 3']
    twitter1 = properties['Twitter 1']
    twitter2 = properties['Twitter 2']
    twitter3 = properties['Twitter 3']
    facebook1 = properties['Facebook 1']
    facebook2 = properties['Facebook 2']
    facebook3 = properties['Facebook 3']
    vkontakte1 = properties['ВКонтакте 1']
    vkontakte2 = properties['ВКонтакте 2']
    vkontakte3 = properties['ВКонтакте 3']
    whatsapp1 = properties['WhatsApp 1']
    whatsapp2 = properties['WhatsApp 2']
    whatsapp3 = properties['WhatsApp 3']
    viber1 = properties['Viber 1']
    viber2 = properties['Viber 2']
    viber3 = properties['Viber 3']
    telegram1 = properties['Telegram 1']
    telegram2 = properties['Telegram 2']
    telegram3 = properties['Telegram 3']
    youtube1 = properties['YouTube 1']
    youtube2 = properties['YouTube 2']
    youtube3 = properties['YouTube 3']
    skype1 = properties['Skype 1']
    skype2 = properties['Skype 2']
    skype3 = properties['Skype 3']
    latitude = properties['Широта']
    longitude = properties['Долгота']
    gis_url = properties['2GIS URL']

    # Insert the data into the table
    insert_query = f"""
        INSERT INTO {table_name} (
            name, description, categories, address, address_comment, postal_code,
            microdistrict, district, city, district_round, region, country, opening_hours,
            timezone, rating, review_count, phone1, phone2, phone3, email1, email2,
            email3, website1, website2, website3, instagram1, instagram2, instagram3,
            twitter1, twitter2, twitter3, facebook1, facebook2, facebook3, vkontakte1,
            vkontakte2, vkontakte3, whatsapp1, whatsapp2, whatsapp3, viber1, viber2,
            viber3, telegram1, telegram2, telegram3, youtube1, youtube2, youtube3,
            skype1, skype2, skype3, latitude, longitude, gis_url, geom
        )
        VALUES (
            %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
            %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
            %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326)
        );
    """
    params = (
        name, description, categories, address, address_comment, postal_code,
        microdistrict, district, city, district_round, region, country, opening_hours,
        timezone, rating, review_count, phone1, phone2, phone3, email1, email2,
        email3, website1, website2, website3, instagram1, instagram2, instagram3,
        twitter1, twitter2, twitter3, facebook1, facebook2, facebook3, vkontakte1,
        vkontakte2, vkontakte3, whatsapp1, whatsapp2, whatsapp3, viber1, viber2,
        viber3, telegram1, telegram2, telegram3, youtube1, youtube2, youtube3,
        skype1, skype2, skype3, latitude, longitude, gis_url
    )
    cursor.execute(insert_query, params)

# Commit the transaction and close the connection
conn.commit()
cursor.close()
conn.close()
