import json
import csv

def convert_geojson_to_csv(input_file, output_file):
    with open(input_file, 'r') as file:
        data = json.load(file)

    features = data['features']

    with open(output_file, 'w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)

        # Write headers
        headers = ['Наименование', 'Описание', 'Рубрики', 'Адрес', 'Комментарий к адресу',
                   'Почтовый индекс', 'Микрорайон', 'Район', 'Город', 'Округ', 'Регион',
                   'Страна', 'Часы работы', 'Часовой пояс', 'Рейтинг', 'Количество отзывов',
                   'Телефон 1', 'Телефон 2', 'Телефон 3', 'E-mail 1', 'E-mail 2', 'E-mail 3',
                   'Веб-сайт 1', 'Веб-сайт 2', 'Веб-сайт 3', 'Instagram 1', 'Instagram 2',
                   'Instagram 3', 'Twitter 1', 'Twitter 2', 'Twitter 3', 'Facebook 1',
                   'Facebook 2', 'Facebook 3', 'ВКонтакте 1', 'ВКонтакте 2', 'ВКонтакте 3',
                   'WhatsApp 1', 'WhatsApp 2', 'WhatsApp 3', 'Viber 1', 'Viber 2', 'Viber 3',
                   'Telegram 1', 'Telegram 2', 'Telegram 3', 'YouTube 1', 'YouTube 2',
                   'YouTube 3', 'Skype 1', 'Skype 2', 'Skype 3', 'Широта', 'Долгота', '2GIS URL']
        writer.writerow(headers)

        # Write data
        for feature in features:
            properties = feature['properties']
            geometry = feature['geometry']
            coordinates = geometry['coordinates']

            row = [properties.get(header, '') for header in headers[:-2]]
            row.extend(coordinates)
            row.append(properties['2GIS URL'])

            writer.writerow(row)

# Usage example
input_file = 'basket.geojson'
output_file = 'basket.csv'
convert_geojson_to_csv(input_file, output_file)
