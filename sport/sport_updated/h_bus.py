import csv
import json

# Read the GeoJSON data
with open('data.json', 'r', encoding='utf-8') as file:
    data = json.load(file)

# Open the CSV file for writing
with open('data.csv', 'w', newline='', encoding='utf-8') as file:
    writer = csv.writer(file)

    # Write the header row
    writer.writerow(['name', 'lat', 'lon'])

    # Iterate over the features
    for feature in data:
        # Extract the necessary information
        name = feature['name']
        lat = feature['point']['lat']
        lon = feature['point']['lon']

        # Write the data to the CSV file
        writer.writerow([name, lat, lon])

print("Conversion completed successfully.")
