import psycopg2
import json

# Database connection parameters
database = "rwh_gis_datalake"
user = "rwh_analytics"
password = "4HPzQt2HyU@"
host = "172.30.227.205"
port = "5439"

# Connect to PostgreSQL
conn = psycopg2.connect(
    database=database,
    user=user,
    password=password,
    host=host,
    port=port
)

# Create a cursor object to execute SQL statements
cursor = conn.cursor()

# Create the table if it does not exist
create_table_query = """
    CREATE TABLE IF NOT EXISTS cemetery_newsss (
        id SERIAL PRIMARY KEY,
        zonename VARCHAR,
        kadnumber VARCHAR,
        zonetype INTEGER,
        styleid VARCHAR,
        shapeleng FLOAT,
        shapearea FLOAT,
        idcemeter INTEGER,
        area FLOAT,
        dist VARCHAR,
        address VARCHAR,
        square FLOAT,
        cadastral VARCHAR,
        post_data VARCHAR,
        year VARCHAR,
        opened VARCHAR,
        free FLOAT,
        muslim FLOAT,
        christian FLOAT,
        mix_religion FLOAT,
        svo FLOAT,
        amount FLOAT,
        marker GEOMETRY(Point, 4326),
        geometry GEOMETRY(MultiPolygon, 4326),
        district VARCHAR
    )
"""
cursor.execute(create_table_query)
conn.commit()

# Load the GeoJSON data
with open('cemetery.geojson') as f:
    data = json.load(f)

# Extract the features from the GeoJSON
features = data['features']

# Insert each feature into the table
for feature in features:
    properties = feature['properties']
    marker = properties['marker']
    geometry = json.dumps(feature['geometry'])

    insert_query = """
        INSERT INTO cemetery_newsss (
            zonename,
            kadnumber,
            zonetype,
            styleid,
            shapeleng,
            shapearea,
            idcemeter,
            area,
            dist,
            address,
            square,
            cadastral,
            post_data,
            year,
            opened,
            free,
            muslim,
            christian,
            mix_religion,
            svo,
            amount,
            marker,
            geometry,
            district
        )
        VALUES (
            %(ZONENAME)s,
            %(KADNUMBER)s,
            %(ZONETYPE)s,
            %(STYLEID)s,
            %(ShapeLeng)s,
            %(ShapeArea)s,
            %(idcemeter)s,
            %(area)s,
            %(dist)s,
            %(address)s,
            %(square)s,
            %(cadastral)s,
            %(post_data)s,
            %(year)s,
            %(opened)s,
            %(free)s,
            %(muslim)s,
            %(christian)s,
            %(mix_religion)s,
            %(svo)s,
            %(amount)s,
            ST_SetSRID(ST_MakePoint(%(marker_lat)s, %(marker_lon)s), 4326),
            ST_SetSRID(ST_GeomFromGeoJSON(%(geometry)s), 4326),
            %(district)s
        )
    """
    cursor.execute(insert_query, {"marker_lat": marker[1], "marker_lon": marker[0], "geometry": geometry, **properties})
    conn.commit()

# Close the cursor and connection
cursor.close()
conn.close()
