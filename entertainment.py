import psycopg2
import json
from psycopg2.extras import execute_values
from geojson import loads

# Database connection parameters
database = "rwh_gis_datalake"
user = "rwh_analytics"
password = "4HPzQt2HyU@"
host = "172.30.227.205"
port = "5439"

# GeoJSON file path
geojson_file = "parks.geojson"

# Connect to the PostGIS database
conn = psycopg2.connect(database=database, user=user, password=password, host=host, port=port)
cur = conn.cursor()

# Read the GeoJSON file
with open(geojson_file) as f:
    data = json.load(f)

# Create the "parks" table if it doesn't exist
cur.execute("""
    CREATE TABLE IF NOT EXISTS parks (
        id SERIAL PRIMARY KEY,
        properties JSONB,
        geom GEOMETRY(MultiPolygon, 4326)
    );
""")

# Prepare the data for batch insertion
batch_data = []
for feature in data["features"]:
    properties = json.dumps(feature["properties"])
    geometry = loads(json.dumps(feature["geometry"]))

    # Convert the geometry to GeoJSON string
    geometry_json = json.dumps(geometry.__geo_interface__)
    batch_data.append((properties, geometry_json))

# Batch insert the features into the "parks" table
execute_values(cur, "INSERT INTO parks (properties, geom) VALUES %s", batch_data, template="(%s, ST_SetSRID(ST_GeomFromGeoJSON(%s), 4326))")

# Commit the transaction and close the connection
conn.commit()
conn.close()
