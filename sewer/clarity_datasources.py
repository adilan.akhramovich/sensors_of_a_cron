import requests
import psycopg2

# API endpoint and token
api_url = 'https://clarity-data-api.clarity.io/v1/datasources'
api_token = 'Z7RuEZeP30XBRI0AYCYYCuduaFiEk6UWfFWvjhOA'

# Database connection details
db_user = 'rwh_analytics'
db_password = '4HPzQt2HyU@'
db_host = '172.30.227.205'
db_port = '5439'
db_name = 'rwh_gis_datalake'

# Table name
table_name = 'clarity_api_datasources'

# Table creation SQL query
create_table_sql = f"""
CREATE TABLE IF NOT EXISTS {table_name} (
    datasource_id VARCHAR(255),
    device_code VARCHAR(255),
    source_type VARCHAR(255),
    name VARCHAR(255),
    "group" VARCHAR(255),
    tags VARCHAR[],
    subscription_status VARCHAR(255),
    subscription_expiration TIMESTAMP
);
"""

# Connect to the PostgreSQL database
conn = psycopg2.connect(
    host=db_host,
    port=db_port,
    database=db_name,
    user=db_user,
    password=db_password
)
cursor = conn.cursor()

# Create the table if it doesn't exist
cursor.execute(create_table_sql)

# Make API call to retrieve data
headers = {'x-api-key': api_token}
response = requests.get(api_url, headers=headers)
data = response.json()

# Insert data into the database
for item in data:
    datasource_id = item['datasourceId']
    device_code = item['deviceCode']
    source_type = item['sourceType']
    name = item.get('name', '')
    group = item.get('group', '')
    tags = item.get('tags', [])
    subscription_status = item['subscriptionStatus']
    subscription_expiration = item['subscriptionExpirationDate']

    # Construct the SQL query
    insert_sql = f"""
    INSERT INTO {table_name} (datasource_id, device_code, source_type, name, "group", tags, subscription_status, subscription_expiration)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
    """

    # Execute the SQL query
    cursor.execute(insert_sql, (datasource_id, device_code, source_type, name, group, tags, subscription_status, subscription_expiration))

# Commit the changes and close the connection
conn.commit()
conn.close()
