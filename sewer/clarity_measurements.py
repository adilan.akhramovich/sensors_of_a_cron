import requests
import gzip
import json

# API endpoint and parameters
api_url = 'https://clarity-data-api.clarity.io/v1/measurements'
token = 'Z7RuEZeP30XBRI0AYCYYCuduaFiEk6UWfFWvjhOA'
headers = {
    'Authorization': f'Token {token}',
    'Accept-Encoding': 'gzip'  # Optional: Enable gzip compression for response payload
}
params = {
    'code': 'A000001',  # Specify the device code or node ID
    'limit': 1000  # Maximum number of measurements to retrieve
}

# Send GET request to the API
response = requests.get(api_url, headers=headers, params=params)

# Check the response status code
if response.status_code == 200:
    # Check if response content is compressed with gzip
    if response.headers.get('Content-Encoding') == 'gzip':
        # Decompress the response content
        decompressed_data = gzip.decompress(response.content).decode('utf-8')
        data = json.loads(decompressed_data)
    else:
        data = response.json()

    # Process the retrieved data
    for measurement in data:
        # Access measurement attributes
        measurement_id = measurement['_id']
        rec_id = measurement['recId']
        device_code = measurement['deviceCode']
        measurement_time = measurement['time']
        location = measurement['location']
        characteristics = measurement['characteristics']

        # Process characteristics
        for characteristic_name, characteristic_data in characteristics.items():
            # Access characteristic values
            value = characteristic_data['value']
            weight = characteristic_data.get('weight')
            raw_value = characteristic_data.get('raw')
            aqi = characteristic_data.get('aqi')
            
            # Perform further processing or store the data as needed

    # Example: Print the first measurement data
    if len(data) > 0:
        print(json.dumps(data[0], indent=4))

else:
    print('Failed to retrieve data. Status code:', response.status_code)
    print('Response content:', response.text)
