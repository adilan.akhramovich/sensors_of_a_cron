# #csv to xlsx Transform
# import pandas as pd

# df = pd.read_excel('sergek_full.xlsx')
# df.to_csv('sergek_full.csv', index=False)


# import geopandas as gpd

# # Read the CSV file into a pandas DataFrame
# df = pd.read_csv('sergek_full.csv')

# # Create a GeoDataFrame from the DataFrame by specifying the geometry column
# gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.longitude, df.latitude))

# # Convert the GeoDataFrame to GeoJSON
# geojson = gdf.to_json()

# # Save the GeoJSON to a file
# with open('sergek_full.geojson', 'w') as file:
#     file.write(geojson)


import pandas as pd
import geopandas as gpd

# Read the Excel file into a pandas DataFrame
df = pd.read_excel('rwh_gis_datalake_public_rep_gis_infra.xlsx')
print(df.columns)

# Create a GeoDataFrame from the DataFrame by specifying the geometry column
geometry = gpd.points_from_xy(df['lon'], df['lat'])
gdf = gpd.GeoDataFrame(df, geometry=geometry)

# Convert the GeoDataFrame to GeoJSON
geojson = gdf.to_json()

# Save the GeoJSON to a file
with open('rwh_gis_datalake_public_rep_gis_infra.geojson', 'w') as file:
    file.write(geojson)

