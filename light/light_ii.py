import psycopg2
import json

# Database connection details
db_user = 'rwh_analytics'
db_password = '4HPzQt2HyU@'
db_host = '172.30.227.205'
db_port = '5439'
db_name = 'rwh_gis_datalake'

# GeoJSON file path
geojson_file = 'Питающие_линии_наружного_освещения.geojson'

# Table name
table_name = 'light_outdoor_lighting_lines'

# Establish a connection to the database
conn = psycopg2.connect(
    user=db_user,
    password=db_password,
    host=db_host,
    port=db_port,
    database=db_name
)

# Create a cursor object to execute SQL queries
cursor = conn.cursor()

# Create the table for the data
create_table_query = f"""
    CREATE TABLE IF NOT EXISTS {table_name} (
        id SERIAL PRIMARY KEY,
        name TEXT,
        voltage TEXT,
        amount_wire TEXT,
        marka_cab TEXT,
        amount_cab FLOAT,
        depth FLOAT,
        district TEXT,
        geom GEOMETRY(MultiLineString, 4326)
    );
"""
cursor.execute(create_table_query)

# Read the GeoJSON file
with open(geojson_file) as file:
    data = json.load(file)

# Iterate over features and insert data into the table
for feature in data['features']:
    properties = feature['properties']
    geometry = feature['geometry']

    # Extract values from properties
    name = properties['NAME']
    voltage = properties['VOLTAGE']
    amount_wire = properties['AMOUNT_WIR']
    marka_cab = properties['MARKA_CAB']
    amount_cab = properties['AMOUNT_CAB']
    depth = properties['DEPTH']
    district = properties['district']
    marker = properties['marker']

    # Construct the parameterized query
    query = f"""
        INSERT INTO {table_name} (name, voltage, amount_wire, marka_cab, amount_cab, depth, district, geom)
        VALUES (%(name)s, %(voltage)s, %(amount_wire)s, %(marka_cab)s, %(amount_cab)s, %(depth)s, %(district)s, 
                ST_SetSRID(ST_GeomFromGeoJSON(%(geometry)s), 4326));
    """

    # Create a parameter dictionary
    params = {
        'name': name,
        'voltage': voltage,
        'amount_wire': amount_wire,
        'marka_cab': marka_cab,
        'amount_cab': amount_cab,
        'depth': depth,
        'district': district,
        'geometry': json.dumps(geometry),
    }

    # Execute the parameterized query
    cursor.execute(query, params)

# Commit the changes and close the cursor and connection
conn.commit()
cursor.close()
conn.close()
